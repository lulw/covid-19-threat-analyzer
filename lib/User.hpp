#ifndef COVID_19_LIB_USER_HPP
#define COVID_19_LIB_USER_HPP

// For std::size_t
#include <cstddef>
#include <string>

#include "GeoPoint.hpp"

/**
* Simple structure to keep user data.
*/
struct User {
    /**
    * User ID - hash from username.
    */
    std::size_t uuid;

    /**
    * Name of the user.
    */
    std::string username;

    /**
    * Current user location.
    */
    GeoPoint location;

    bool operator==(const User& rhs) {
        return this->uuid == rhs.uuid;
    }
};

#endif  // COVID_19_LIB_USER_HPP