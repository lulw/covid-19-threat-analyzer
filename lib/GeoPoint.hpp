#ifndef COVID_19_LIB_GEOPOINT_HPP
#define COVID_19_LIB_GEOPOINT_HPP

/**
* GeoPoint - Representation of Earth Coordinate Point.
*/
struct GeoPoint {
    /**
    * Latitude in degrees 
    */
    double latitude;

    /**
    * Longitude in degrees 
    */
    double longitude;
};

#endif  // COVID_19_LIB_GEOPOINT_HPP