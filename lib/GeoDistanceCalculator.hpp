#ifndef COVID_19_LIB_GEODISTANCECALCULATOR_HPP
#define COVID_19_LIB_GEODISTANCECALCULATOR_HPP

#include "GeoPoint.hpp"

/**
* Distance Calculator
* Helper class to simplyfy calulations and convertions.
*/
class GeoDistanceCalculator {
 public:
    /**
    * deg2rad - convert degrees to radians.
    * @param deg - degress 
    * @return radians - degress value converted to radians.
    */
    double deg2rad(double deg);

    /**
    * deg2rad - convert degrees to radians.
    * @param deg - degress 
    * @return radians - degress value converted to radians.
    */
    double rad2deg(double rad);

    /**
    * Calculate distance in km between two earth coordinates.
    * Uses haversine formula to calculate distance between two points.
    * @param geoPointFrom : latitude and longitude in degrees.
    * @param geoPointTo : latitude and longitude in degrees.
    * return distance in km.
    */
    double distance(const GeoPoint &geoPointFrom, const GeoPoint &geoPointTo);
};


#endif  // COVID_19_LIB_GEODISTANCECALCULATOR_HPP