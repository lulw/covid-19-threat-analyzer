#include "GeoDistanceCalculator.hpp"

#include <cmath>
#include <math.h>

double GeoDistanceCalculator::deg2rad(double deg) {
    return (deg * M_PI / 180);
}

double GeoDistanceCalculator::rad2deg(double rad) {
    return (rad * 180 / M_PI);
}

double GeoDistanceCalculator::distance(const GeoPoint &geoPointFrom, const GeoPoint &geoPointTo) {
    constexpr double earthRadiusKm = 6371.0;
    double lat1r, lon1r, lat2r, lon2r, u, v;
    lat1r = deg2rad(geoPointFrom.latitude);
    lon1r = deg2rad(geoPointFrom.longitude); 
    lat2r = deg2rad(geoPointTo.latitude);
    lon2r = deg2rad(geoPointTo.longitude);
    u = sin((lat2r - lat1r)/2);
    v = sin((lon2r - lon1r)/2);
    return 2.0 * earthRadiusKm * asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v));
}