#include "Covid19ThreatAnalyzer.hpp"
#include <iostream>


void Covid19ThreatAnalyzer::addUser(User user) {
    users.emplace_back(user);  
}

VectorOfUsersInThreat Covid19ThreatAnalyzer::getUsersInThreat() {
    // Initial idea is simplified just to list user too close to each other
    VectorOfUsersInThreat usersInThreat;

    for (int i = 0; i < users.size(); i++) {
        for (int j = i; j < users.size(); j++) {
            if (i == j) {
                continue;
            }
            auto distance = geoDistanceCalulator.distance(users[i].location, users[j].location);
            if (distance <= minDistance) {
                usersInThreat.emplace_back(std::make_pair(users[i], users[j]));
            }
        }
    }
    
    return usersInThreat;
}