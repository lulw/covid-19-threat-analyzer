#ifndef COVID_19_LIB_COVID19THREATANALYZER_HPP
#define COVID_19_LIB_COVID19THREATANALYZER_HPP

#include "User.hpp"
#include "GeoDistanceCalculator.hpp"

#include <vector>
#include <map>
#include <utility>

typedef std::vector<User> VectorOfUsers;
typedef std::vector<std::pair<User, User>> VectorOfUsersInThreat;

/**
* Potential Covid-19 thread detector.
*/
class Covid19ThreatAnalyzer {
    GeoDistanceCalculator geoDistanceCalulator;
    VectorOfUsers users;
    /**
    * Recommended minnimum distance between users.
    */
    const double minDistance = 0.0015;

 public:
    /**
    * Add user to observed ones.
    */
    void addUser(User user);

    /**
    * Check which users might be in threat because of close contact.
    * @return vector with users where distance between is less than minDistance.
    */
    VectorOfUsersInThreat getUsersInThreat();
};

#endif  // COVID_19_LIB_COVID19THREATANALYZER_HPP