# Compilation
```bash
mkdir build && cd build
conan install ..
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
make test
```

## search for conan deps
```
conan search <name> --remote=conan-center
```
