#include <catch2/catch.hpp>

// For std::hash
#include <functional>

#include "GeoPoint.hpp"
#include "GeoDistanceCalculator.hpp"
#include "Covid19ThreatAnalyzer.hpp"

using Catch::Matchers::Matches;

SCENARIO("Dont mess with Chuck Norris") {
    GIVEN("Covid19ThreatAnalyzer with 3 users") {
        Covid19ThreatAnalyzer c19ThreatAnalyzer;
        c19ThreatAnalyzer.addUser({
            std::hash<std::string>{}("You"), 
            "You", 
            {52.403648, 16.915979},
        });
        c19ThreatAnalyzer.addUser({
            std::hash<std::string>{}("Janusz Gajos"), 
            "Janusz Gajos", 
            {55.403648, 16.915979},
        });
        c19ThreatAnalyzer.addUser({
            std::hash<std::string>{}("Chuck Norris"), 
            "Chuck Norris", 
            {52.403658, 16.915979},
        });
        WHEN("Check for possible threats occur") {
            auto usersInThreat = c19ThreatAnalyzer.getUsersInThreat();

            THEN("You should enlarge your distance to Chuck Norris") {
                REQUIRE( usersInThreat.size() == 1 );
                REQUIRE_THAT( usersInThreat[0].first.username, Matches("You") );
                REQUIRE_THAT( usersInThreat[0].second.username, Matches("Chuck Norris") );
            }
        }
    }
}
