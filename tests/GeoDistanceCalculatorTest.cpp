#include <catch2/catch.hpp>

#include "GeoPoint.hpp"
#include "GeoDistanceCalculator.hpp"

TEST_CASE("Disance between two geo points in km") {
	GeoPoint gp1 = {55.03682, -1.70989};
	GeoPoint gp2 = {40.47913, -117.69940};
	GeoDistanceCalculator gdc;
	REQUIRE(gdc.distance(gp1, gp2) == Approx(7790.55));
}

TEST_CASE("Disance between two the same points") {
	GeoPoint gp1 = {55.03682, -1.70989};
	GeoPoint gp2 = {55.03682, -1.70989};
	GeoDistanceCalculator gdc;
	REQUIRE(gdc.distance(gp1, gp2) == Approx(0.0));
}

TEST_CASE("Small Distance") {
	GeoPoint gp1 = {0.0, 0.0};
	GeoPoint gp2 = {0.00001, 0.00001};
	GeoDistanceCalculator gdc;
	REQUIRE(gdc.distance(gp1, gp2) == Approx(0.001572).margin(6));
}

TEST_CASE("Large distance from PSI to somewhere") {
	GeoPoint gp1 = {52.403648, 16.915979};
	GeoPoint gp2 = {57.875435,-136.306000};
	GeoDistanceCalculator gdc;
	REQUIRE(gdc.distance(gp1, gp2) == Approx(7514.61));
}

TEST_CASE("Large distance") {
	GeoPoint gp1 = {90.0, -180};
	GeoPoint gp2 = {-90.0, -180.0};
	GeoDistanceCalculator gdc;
	REQUIRE(gdc.distance(gp1, gp2) == Approx(20015.09));
}

TEST_CASE("Earth turn") {
	GeoPoint gp1 = {0.0, 0.0};
	GeoPoint gp2 = {180.0, 180.0};
	GeoDistanceCalculator gdc;
	REQUIRE(gdc.distance(gp1, gp2) == Approx(0.0));
}