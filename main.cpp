#include <iostream>

// For std::hash
#include <functional>

#include "GeoPoint.hpp"
#include "GeoDistanceCalculator.hpp"
#include "Covid19ThreatAnalyzer.hpp"

int main() {
	Covid19ThreatAnalyzer c19ThreatAnalyzer;

	c19ThreatAnalyzer.addUser({
		std::hash<std::string>{}("You"), 
		"You", 
		{52.403648, 16.915979},
	});

	c19ThreatAnalyzer.addUser({
		std::hash<std::string>{}("Marek Konrad"), 
		"Marek Konrad", 
		{53.403648, 16.915979},
	});

	c19ThreatAnalyzer.addUser({
		std::hash<std::string>{}("Grzegorz Ciechowski"), 
		"Grzegorz Ciechowski", 
		{54.403648, 16.915979},
	});

	c19ThreatAnalyzer.addUser({
		std::hash<std::string>{}("Janusz Gajos"), 
		"Janusz Gajos", 
		{55.403648, 16.915979},
	});

	c19ThreatAnalyzer.addUser({
		std::hash<std::string>{}("Chuck Norris"), 
		"Chuck Norris", 
		{52.403658, 16.915979},
	});

	auto usersInThreat = c19ThreatAnalyzer.getUsersInThreat();
	for (auto &uit : usersInThreat) {
		std::cout 
			<< "Covid-19 Thread detected! "
			<< uit.first.username 
			<< " are too close to "
			<< uit.second.username
			<< "!!! Please enlarge your distance to min 1.5m ... "
			<< std::endl;
	}


	return 0;
}

